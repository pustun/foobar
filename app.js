/**
 * Created by Oleksandr on 10/24/2015.
 */
angular.module('myApp', [])
    .controller('ListCtrl', function($scope) {
        $scope.items = [
            {
                "name": "Dance station",
                "avatar": "http://www.dance-station.com/files/cto_layout/img/logo-free.png",
                "location":"48.1198185,11.5728228"
            },
            {
                "name": "Fit Star",
                "avatar": "http://www.fit-star.de/wp-content/uploads/2015/03/Fit_star_logo_70px_2.png",
                "location":"48.1514031,11.5557799"
            },
            {
                "name": "Almeya",
                "avatar": "http://static.wixstatic.com/media/3f976d_2c17f75d8fd6d1ca647ba54d9b36e999.jpg_srz_p_115_237_75_22_0.50_1.20_0.00_jpg_srz",
                "location":"49.992506,36.2311903"
            }
        ];
    });