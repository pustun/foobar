angular.module('myApp', [])
    .factory('institutionService', function () {
        var institution =  {
            "name": "Dance Station",
            "background": "https://scontent-vie1-1.xx.fbcdn.net/hphotos-xaf1/v/t1.0-9/432103_10150642633756506_1888369779_n.jpg?oh=0d41180babac125d9f74318e3765c94a&oe=56C85A3D",
            "location": "48.1198185,11.5728228",
            "city": "München",
            "address": "Humboldtstraße 29 81543",
            "site": "www.dance-station.com",
            "email": "info@dance-station.com",
            "phone": ["089 - 6511 4404", "089 - 6511 4505"],
            "description": "Willkommen bei deiner Tanzschule in München. Wir bieten Kurse sowohl für Anfänger als auch für Fortgeschrittene, für jung, für alt – einfach für alle die Spaß und Freude an Tanz und Bewegung haben!",
            "gallery": [
                "https://scontent-vie1-1.xx.fbcdn.net/hphotos-xfa1/t31.0-8/11802728_10153502389511506_3278760433678584027_o.jpg",
                "https://scontent-vie1-1.xx.fbcdn.net/hphotos-xfa1/v/t1.0-9/11753725_10153502388191506_7110057077497391275_n.jpg?oh=9895fd39e8857dd0cc23c9613ccdc851&oe=56C23761",
                "https://scontent-vie1-1.xx.fbcdn.net/hphotos-xap1/t31.0-8/856799_10151440705806506_977317844_o.jpg",
                "https://scontent-vie1-1.xx.fbcdn.net/hphotos-xfa1/v/t1.0-9/424414_10150654287836506_1234492742_n.jpg?oh=31971086e04eddc4ba10bc63e2be141d&oe=56BFA202",
                "https://scontent-vie1-1.xx.fbcdn.net/hphotos-xap1/v/t1.0-9/207974_10151149227481506_2024239469_n.jpg?oh=247f8298c50800e3b3f69c4a4a8fd33d&oe=5688FD69"
            ],
            "schedule": {
                "days": ["MO", "DI", "MI", "DO", "FR", "SA", "SO"],
                "rooms": [
                    {
                        "name": "Studio1",

                        "days_schedule": {
                            "MO": [
                                {
                                    "name": "Modern Jazz I",
                                    "teacher": "Volker",
                                    "from": "1730",
                                    "to": "1830"
                                },
                                {
                                    "name": "Ballett II",
                                    "teacher": "Beatrice",
                                    "from": "1835",
                                    "to": "2005"
                                },
                                {
                                    "name": "Jazz Basic",
                                    "teacher": "Kathi",
                                    "from": "2010",
                                    "to": "2110"
                                },
                                {
                                    "name": "Body Powerjam",
                                    "teacher": "Patrick",
                                    "from": "2115",
                                    "to": "2230"
                                }
                            ],
                            "DI": [
                                {
                                    "name": "Ballett II",
                                    "teacher": "Natalia",
                                    "from": "1830",
                                    "to": "1930"
                                },
                                {
                                    "name": "Jazz I",
                                    "teacher": "Angi",
                                    "from": "1935",
                                    "to": "2050"
                                },
                                {
                                    "name": "Jivamukti Yoga I-II",
                                    "teacher": "Annette",
                                    "from": "2255",
                                    "to": "2110"
                                }
                            ],
                            "MI": [
                                {
                                    "name": "Ballett I",
                                    "teacher": "Ina",
                                    "from": "1820",
                                    "to": "1935"
                                },
                                {
                                    "name": "Lyrical Jazz I",
                                    "teacher": "Sinah",
                                    "from": "1940",
                                    "to": "2055"
                                },
                                {
                                    "name": "Ballett Barre Fitness",
                                    "teacher": "Sinah",
                                    "from": "2100",
                                    "to": "2200"
                                }
                            ],
                            "DO": [
                                {
                                    "name": "Hip Hop Teens (11-13 J.)",
                                    "teacher": "Laura Del Vecchio",
                                    "from": "1615",
                                    "to": "1715"
                                },
                                {
                                    "name": "Ballett GK",
                                    "teacher": "Aya Sonne",
                                    "from": "1715",
                                    "to": "1830"
                                },
                                {
                                    "name": "Club Moves",
                                    "teacher": "Live DJ mit Patrick",
                                    "from": "1835",
                                    "to": "1935"
                                },
                                {
                                    "name": "Modern Jazz II",
                                    "teacher": "Volker",
                                    "from": "1940",
                                    "to": "2250"
                                },
                                {
                                    "name": "Modern Dance I",
                                    "teacher": "Maximilian",
                                    "from": "2100",
                                    "to": "2250"
                                }
                            ],
                            "FR": [
                                {
                                    "name": "Classical Work Out",
                                    "teacher": "Ina",
                                    "from": "1640",
                                    "to": "1740"
                                },
                                {
                                    "name": "Ballett I",
                                    "teacher": "Laura Cannarozzo",
                                    "from": "1745",
                                    "to": "1900"
                                },
                                {
                                    "name": "Lyrical Jazz II",
                                    "teacher": "Mathias",
                                    "from": "1905",
                                    "to": "2020"
                                },
                                {
                                    "name": "Salsa Lady Style",
                                    "teacher": "Laura Del Vecchio",
                                    "from": "2025",
                                    "to": "2140"
                                }
                            ],
                            "SA": [
                                {
                                    "name": "Ballett I-II",
                                    "teacher": "Natalia",
                                    "from": "1130",
                                    "to": "1300"
                                },
                                {
                                    "name": "Lyrical Jazz I",
                                    "teacher": "Mathias",
                                    "from": "1305",
                                    "to": "1420"
                                },
                                {
                                    "name": "Ballett GK I",
                                    "teacher": "Alexander",
                                    "from": "1425",
                                    "to": "1540"
                                },
                                {
                                    "name": "Modern Dance I",
                                    "teacher": "Pedro",
                                    "from": "1545",
                                    "to": "1700"
                                },
                                {
                                    "name": "Modern Dance GK o. Vork.",
                                    "teacher": "Pedro",
                                    "from": "1705",
                                    "to": "1820"
                                }
                            ],
                            "SO": [
                                {
                                    "name": "Modern Dance GK",
                                    "teacher": "Valerio",
                                    "from": "1135",
                                    "to": "1250"
                                },
                                {
                                    "name": "Jazz I",
                                    "teacher": "Kathi",
                                    "from": "1255",
                                    "to": "1410"
                                },
                                {
                                    "name": "Ballett I-II",
                                    "teacher": "Alexander",
                                    "from": "1415",
                                    "to": "1530"
                                },
                                {
                                    "name": "Lyrical Jazz II",
                                    "teacher": "Mathias",
                                    "from": "1535",
                                    "to": "1705"
                                },
                                {
                                    "name": "Broadway Musical Jazz GK",
                                    "teacher": "Patrick",
                                    "from": "1710",
                                    "to": "1840"
                                }
                            ]
                        }
                    },
                    {
                        "name": "Studio2",
                        "days_schedule": {
                            "MO": [
                                {
                                    "name": "Hip Hop Kids (7-9 J.)",
                                    "teacher": "Linh",
                                    "from": "1600",
                                    "to": "1700"
                                },
                                {
                                    "name": "Commercial Jazz I",
                                    "teacher": "Patrick",
                                    "from": "1745",
                                    "to": "1915"
                                },
                                {
                                    "name": "Modern Dance II",
                                    "teacher": "Volker",
                                    "from": "1920",
                                    "to": "2035"
                                },
                                {
                                    "name": "Lyrical Jazz I",
                                    "teacher": "Fabi",
                                    "from": "2040",
                                    "to": "2155"
                                }
                            ],
                            "DI": [
                                {
                                    "name": "MiniBallett Neueinsteiger(4-5 J.)",
                                    "teacher": "Laura Wetterau",
                                    "from": "1445",
                                    "to": "1530"
                                },
                                {
                                    "name": "MiniBallett I (4-5 J.)",
                                    "teacher": "Laura Wetterau",
                                    "from": "1530",
                                    "to": "1615"
                                },
                                {
                                    "name": "MiniBallett II (6-7 J.)",
                                    "teacher": "Laura Wetterau",
                                    "from": "1615",
                                    "to": "1700"
                                },
                                {
                                    "name": "Hip Hop Streetstyle I",
                                    "teacher": "Maxi",
                                    "from": "1800",
                                    "to": "1900"
                                },
                                {
                                    "name": "Afro Jazz I-II",
                                    "teacher": "Patrick",
                                    "from": "1905",
                                    "to": "2020"
                                },
                                {
                                    "name": "Ballett GK o. Vork",
                                    "teacher": "Ina",
                                    "from": "2025",
                                    "to": "2140"
                                }
                            ],
                            "MI": [
                                {
                                    "name": "Ballett Kids (8-10 J.)",
                                    "teacher": "Ina",
                                    "from": "1530",
                                    "to": "1615"
                                },
                                {
                                    "name": "Hip Hop Kids (10-13 J.)",
                                    "teacher": "Linh",
                                    "from": "1615",
                                    "to": "1715"
                                },
                                {
                                    "name": "Hip Hop II",
                                    "teacher": "Jacqueline",
                                    "from": "1835",
                                    "to": "1935"
                                },
                                {
                                    "name": "Modern Dance",
                                    "teacher": "Maximilian",
                                    "from": "1940",
                                    "to": "2055"
                                },
                                {
                                    "name": "Zumba",
                                    "teacher": "Jacqueline",
                                    "from": "2100",
                                    "to": "2200"
                                }
                            ],
                            "DO": [
                                {
                                    "name": "Ballett Teens (12-14 J.)",
                                    "teacher": "Ina",
                                    "from": "1645",
                                    "to": "1745"
                                },
                                {
                                    "name": "Modern Jazz I",
                                    "teacher": "Volker",
                                    "from": "1750",
                                    "to": "1905"
                                },
                                {
                                    "name": "Salsa Ladystyle",
                                    "teacher": "Laura Del Vecchio",
                                    "from": "1910",
                                    "to": "2010"
                                },
                                {
                                    "name": "Yoga GK",
                                    "teacher": "Steven",
                                    "from": "2015",
                                    "to": "2130"
                                }
                            ],
                            "FR": [
                                {
                                    "name": "Funk Jazz Teens (13-15 J.)",
                                    "teacher": "Linh",
                                    "from": "1515",
                                    "to": "1615"
                                },
                                {
                                    "name": "Hip Hop Teens (14-17 J.) ",
                                    "teacher": "Maxi",
                                    "from": "1615",
                                    "to": "1715"
                                },
                                {
                                    "name": "Contemporary",
                                    "teacher": "Aya",
                                    "from": "1800",
                                    "to": "1920"
                                },
                                {
                                    "name": "Hip Hop I",
                                    "teacher": "Laura del Vechio",
                                    "from": "1920",
                                    "to": "2020"
                                },
                                {
                                    "name": "House Dance",
                                    "teacher": "Jasin",
                                    "from": "2025",
                                    "to": "2125"
                                }
                            ],
                            "SA": [
                                {
                                    "name": "Ballett GK",
                                    "teacher": "Rafaela",
                                    "from": "1100",
                                    "to": "1215"
                                },
                                {
                                    "name": "Modern Floorwork GK I",
                                    "teacher": "Valerio",
                                    "from": "1215",
                                    "to": "1315"
                                },
                                {
                                    "name": "Jazz Dance GK (o. Vorkenntn.)",
                                    "teacher": "Sinah",
                                    "from": "1320",
                                    "to": "1435"
                                },
                                {
                                    "name": "HipHop GK (Erwachsene)",
                                    "teacher": "Jacqueline",
                                    "from": "1440",
                                    "to": "1540"
                                }
                            ],
                            "SO": [
                                {
                                    "name": "Afro für Alle",
                                    "teacher": "Elliot (mit Livemusik)",
                                    "from": "1135",
                                    "to": "1250"
                                },
                                {
                                    "name": "Jivamukti Yoga Basic",
                                    "teacher": "Annette",
                                    "from": "1255",
                                    "to": "1410"
                                },
                                {
                                    "name": "Ballett GK o. Vork.",
                                    "teacher": "Ulrich",
                                    "from": "1415",
                                    "to": "1530"
                                },
                                {
                                    "name": "Zumba",
                                    "teacher": "Laura Del Vecchio",
                                    "from": "1535",
                                    "to": "1650"
                                }
                            ]
                        }
                    }
                ]
            }

        };

        return {
            getInstitution: function() {
                return institution;
            }
        }
    })
    .controller('ListCtrl', function ($scope, institutionService) {
        function mapServerData2ViewModel (serverData) {
            for(var i in serverData.schedule.rooms) {
                var room = serverData.schedule.rooms[i];
                for(var day in room.days_schedule) {

                    var dayClasses = room.days_schedule[day];
                    var offset = getOffset(day, serverData.schedule.rooms);
                    for(var j in dayClasses) {
                        var clazz = dayClasses[j];
                        clazz.offset = clazz.from - offset;
                        clazz.height = clazz.to - clazz.from;
                        offset = clazz.to;
                    }
                }
            }
            return serverData;
        }
        function getOffset(day, rooms) {
            var offset = 2359;
            for(var i in rooms) {
                var firstClass = rooms[i].days_schedule[day];
                if(firstClass != undefined) {

                    if(offset > firstClass[0].from) {
                        offset = firstClass[0].from;
                    }
                }
            }
            return offset;
        }

        var serverData = institutionService.getInstitution();

        var viewModel = mapServerData2ViewModel(serverData);

        $scope.institution = viewModel;
    });